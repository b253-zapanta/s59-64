import { Card, Col } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ItemCard({ item }) {
  const { _id, name, stock } = item;

  return (
    <Col lg={1}>
      <Card className="my-3 text-center" style={{ width: "15rem" }}>
        <Card.Body>
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle className="text-muted">Stock:</Card.Subtitle>
          <Card.Text>{stock}</Card.Text>
          <Link className="btn btn-dark" size="sm" to={`/items/${_id}`}>
            View Item
          </Link>
        </Card.Body>
      </Card>
    </Col>
  );
}
