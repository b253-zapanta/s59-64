import { useEffect, useState } from "react";
import ItemCard from "../components/ItemCard";
import { Container, Card, Row, Col } from "react-bootstrap";

//env
const renderUrl = process.env.REACT_APP_RENDER_URL;
const localUrl = process.env.REACT_APP_LOCAL_URL;
const mode = process.env.REACT_APP_MODE;
const link = mode === "DEV" ? localUrl : renderUrl;

// import mock database
export default function Supplies() {
  const [item, setItems] = useState([]);

  useEffect(() => {
    fetch(`${link}/api/availableSupplies`)
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        setItems(
          data.data.map((item) => {
            return <ItemCard key={item._id} item={item} />;
          })
        );
      });
  }, []);

  return (
    <Container className="p-5 mt-5">
      <Row>
        <Col md={8}>
          <h1>Supplies</h1>
          {item}
        </Col>

        <Col md={4}>
          <h1>Requests</h1>
          <Card style={{ width: "18rem" }}>
            <Card.Body>
              <Card.Title>Card Title</Card.Title>
              <Card.Subtitle className="mb-2 text-muted">
                Card Subtitle
              </Card.Subtitle>
              <Card.Text>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </Card.Text>
              <Card.Link href="#">Card Link</Card.Link>
              <Card.Link href="#">Another Link</Card.Link>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
