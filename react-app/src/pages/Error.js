import { Nav, Row, Col } from "react-bootstrap";
import { NavLink } from "react-router-dom";
export default function Error() {
  return (
    <Row>
      <Col className="p-5">
        <h1>Page Not Found</h1>
        <Nav.Link as={NavLink} to="/" exact>
          Go back to <span className="text-primary">homepage.</span>
        </Nav.Link>
      </Col>
    </Row>
  );
}
